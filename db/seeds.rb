# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



insurances = Insurance.create ([{ name: 'JumpMan Cross' , street_address: '23 Chicago Avenue'},
                                { name: 'Red Cross Plan' , street_address: '1500 Red River'},
                                { name: 'Texas UnitedHealth' , street_address: '2355 Texas Way'},
                                { name: 'UH Life Support' , street_address: '17690 Cougar Street'},
                                { name: 'Screwstone Health Plan' , street_address: '713 Lean Street'}])

specialists = Specialist.create([{ name: 'Rachel Roth', specialty:'Dentist' },
                                 { name: 'David Brown', specialty:'Doctor' },
                                 { name: 'Marisol White', specialty:'Nurse' },
                                 { name: 'Tyrone Wayne', specialty:'Orthopedist' },
                                 { name: 'Daniel Lao', specialty:'Optometrist' }])

patients = Patient.create([{ name: 'Jay Bustos', street_address: '243 Huckleberry Drive', :insurance_id => 1 },
                           { name: 'Tyrese Gibbs', street_address: '5523 SouthPark Street', :insurance_id => 2 },
                           { name: 'Perla Shelton', street_address: '1390 WestBerry Ave', :insurance_id => 3 },
                           { name: 'Diane Yoo', street_address: '145 Red Ave', :insurance_id => 4 },
                           { name: 'Bobby Hill', street_address: '23500 King Hill', :insurance_id => 5 }])

Appointment.create( :specialist_id => 1, :patient_id => 1,complaint: 'Gum disease', :appointment_date =>  '2014-12-12' , fee: 56.99)
Appointment.create( :specialist_id => 2, :patient_id => 1, complaint: 'Throwing up', :appointment_date =>  '2014-10-3', fee: 89.67)
Appointment.create( :specialist_id => 1, :patient_id => 2, complaint: 'Tooth hurting ', :appointment_date => '2014/11/30', fee: 156.90)
Appointment.create( :specialist_id => 3, :patient_id => 2, complaint: 'Feeling sick', :appointment_date =>  '2014-12-21', fee: 100)
Appointment.create( :specialist_id => 3, :patient_id => 3, complaint: 'Checking heart pulse', :appointment_date =>  '2014-11-17', fee: 212.45)
Appointment.create( :specialist_id => 1, :patient_id => 3, complaint: 'Drug test', :appointment_date =>  '2014-10-25', fee: 145.99)
Appointment.create( :specialist_id => 4, :patient_id => 3, complaint: 'Leg surgery', :appointment_date =>  '2014-10-30', fee: 155.0)
Appointment.create( :specialist_id => 5, :patient_id => 4, complaint: 'Need Contact lenses', :appointment_date =>  '2014-11-05', fee: 112.80)
Appointment.create( :specialist_id => 2, :patient_id => 4, complaint: 'Blood tests', :appointment_date =>  '2014-11-18', fee: 140.34)
Appointment.create( :specialist_id => 4, :patient_id => 5, complaint: 'X=ray for broken leg ', :appointment_date =>  '2014-12-24', fee: 235)
Appointment.create( :specialist_id => 5, :patient_id => 5, complaint: 'Vision check up', :appointment_date =>  '2014-10-29', fee: 123.45)
